import socket
import struct
import sys
import select
import netifaces as ni
import threading
from enum import Enum

MSG_PEEK = 2
PORT = 22222
turnedDown = False
multicast = "225.0.0.250"
empty_message = b''

class Modes(Enum):
    broadcast = 1
    multicast = 2

class PacketType(Enum):
    message = 1
    echo_request = 2
    echo_response = 3

mode = Modes.broadcast

def get_packet(type, data):
    return bytes(str(type.value), "utf8") + data

def receive_messages(udp_socket):
    while not turnedDown:
        ready = select.select([udp_socket], [], [], 1)
        if len(ready[0]) == 0:
            continue
        packet, address = udp_socket.recvfrom(64)
        packet_type, packet = int(packet[0:1].decode("utf8")), packet[1:]
        if packet_type == PacketType.echo_request.value:
            udp_socket.sendto(get_packet(PacketType.echo_response, empty_message),  address)
        elif packet_type == PacketType.echo_response.value:
            print("Client {}".format(address[0]))
        else:
            print("{}: {}".format(address[0], packet.decode("utf8")))

if __name__ == '__main__':
    # 1st arg - interface name
    config = ni.ifaddresses(sys.argv[1])[ni.AF_INET][0]
    if (len(sys.argv) > 2): multicast = sys.argv[2]
    netmask = config['netmask']
    address = config['addr']
    broadcast = config['broadcast']
    print("netmask: {}, address: {}, broadcast: {}".format(netmask, address, broadcast))

    membership_request = struct.pack("4s4s", socket.inet_aton(multicast), socket.inet_aton(address))

    udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
    udp_socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    udp_socket.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_IF, socket.inet_aton(address))
    udp_socket.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_LOOP, 1)

    udp_socket.bind(('', PORT))

    receive_thread = threading.Thread(target=receive_messages, args=[udp_socket])
    receive_thread.start()

    udp_socket.sendto(get_packet(PacketType.echo_request, empty_message), (broadcast, PORT))

    while True:
        input_str = str(input())
        if input_str == "exit":
            turnedDown = True
            receive_thread.join()
            exit()
        elif input_str == "switch":
            mode = Modes.broadcast if mode == Modes.multicast else Modes.multicast
            print("switched to {}".format(mode))
        elif input_str == "leave":
            try:
                udp_socket.setsockopt(socket.IPPROTO_IP, socket.IP_DROP_MEMBERSHIP, membership_request)
                print("Left from multicast group {}".format(multicast))
            except:
                print("wow")
        elif input_str == "join":
            try:
                udp_socket.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, membership_request)
                print("Joined to multicast group {}".format(multicast))
            except:
                print("wow")
        elif input_str == "show all":
            udp_socket.sendto(get_packet(PacketType.echo_request, empty_message), (broadcast, PORT))
        else:
            packet = get_packet(PacketType.message, bytes(input_str, "utf8"))
            if mode == Modes.broadcast:
                udp_socket.sendto(packet, (broadcast, PORT))
            else:
                udp_socket.sendto(packet, (multicast, PORT))

