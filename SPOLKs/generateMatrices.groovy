def clearFile = { file ->
    PrintWriter writer = new PrintWriter(file);
    writer.print("");
    writer.close();
}

file1Name = "matrix_a.txt"
file2Name = "matrix_b.txt"
def size
if(args){
    size = args[0].toInteger()
} else {
    size = 10
    println "Size set to 10 (default)"
}

file1 = new File(file1Name)
file2 = new File(file2Name)
random = new Random()
clearFile file1
clearFile file2
size.times {
    size.times {
        file1 << random.nextInt() << " "
        file2 << random.nextInt() << " "
    }
    file1 << "\n"
    file2 << "\n"
}

println "Done."

