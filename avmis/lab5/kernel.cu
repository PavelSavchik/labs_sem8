﻿#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/types.h>
#include <time.h>
#include <math.h>
#include <cuda.h>
#include <curand.h>
#include <curand_kernel.h>
#include <iostream>
#include <chrono>

using namespace std;

#define POW_2(x) ((x)*(x))
#define abs(x) ((x < 0) ? (-x) : x)

#define DATA_FILE "data.txt"

#define N 10000
#define M 100

#define K 25

#define MAX_THREADS 128
#define MAX_BLOCKS 1000

#define STREAM_COUNT 2

//UTILS
class Timer{
private:
	chrono::system_clock timer = chrono::high_resolution_clock();
	chrono::system_clock::time_point _start, _end;
	long _sum;
public:
	Timer(){
		_sum = 0;
	}

	void start()
	{
		_start = timer.now();
		
	}

	long end()
	{
		_end = timer.now();
		chrono::duration<float> result = _end - _start;
		chrono::milliseconds ms = chrono::duration_cast<chrono::milliseconds>(result);
		return ms.count();
	}

	void logEnd(char *message)
	{
		auto result = end();
		cout << message << " : " << result << endl;
	}

	void sum() {
		auto result = end();
		_sum += result;
	}

	void showSum() {
		cout << _sum << endl;
	}
};

class TimerCuda{
private:
	cudaEvent_t _start, _stop;
	float _total;
public:
	TimerCuda(){
		cudaEventCreate(&_start);
		cudaEventCreate(&_stop);
		_total = 0;
	}

	void start()
	{
		cudaEventRecord(_start);
		cudaEventSynchronize(_start);
	}

	float stop()
	{
		float ms = 0;
		cudaEventRecord(_stop);
		cudaEventSynchronize(_stop);
		cudaEventElapsedTime(&ms, _start, _stop);
		_total += ms;
		return ms;
	}
	
	float logStop(char *message)
	{
		float ms = stop();
		cout << message << " : " << ms << endl;
		return ms;
	}

	float getTotal()
	{
		return _total;
	}

	float logTotal(char *message)
	{
		cout << message << " : " << getTotal() << endl;
		return getTotal();
	}
};

double* readData()
{
	FILE *file = fopen(DATA_FILE, "r");
	char buff[64];
	int i, j;

	double *a = (double *)malloc(N*M*sizeof(double));

	for (i = 0; i < N; i++)
	{
		for (j = 0; j < M; j++)
		{
			double v;

			fscanf(file, "%lf", &v);

			a[i*M + j] = v;
		}
	}

	fclose(file);

	return a;
}

////////////////////////////////////////////////////////////////////////////////
// CUDA

__global__ void divArrayGPU(double *data, int n, int *counts)
{
	unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;
	
	if (i < n && counts[blockIdx.x] != 0)
	{
		data[i] /= counts[blockIdx.x];
	}
}

__device__ double calcDistanceDevice(double *point_1, double *point_2, int m)
{
	double distance = 0;

	for (auto attr = 0; attr < m; attr++) {
		distance += POW_2(point_1[attr] - point_2[attr]);
	}

	return sqrt(distance);
}

//returns results
__global__ void getClassesForPoints(int *results, double *etalons, double *data, int n, int m, int k)
{
	//stores the distance from the point to the each class
	extern __shared__ double shared_data[];

	auto point_index = blockIdx.x * blockDim.x + threadIdx.x;
	auto classIdex = threadIdx.y;
	
	if (point_index >= n) return;

	auto shared_data_index = threadIdx.x * k;

	shared_data[shared_data_index + classIdex] = calcDistanceDevice(data + point_index * m, etalons + classIdex * m, m);

	__syncthreads();

	if (classIdex != 0) return;

	auto min_class_index = 0;
	auto min_distance = shared_data[shared_data_index];
	for (auto classIndex = 1; classIndex < k; classIndex++)
	{
		if (shared_data[shared_data_index + classIndex] < min_distance)
		{
			min_distance = shared_data[shared_data_index + classIndex];
			min_class_index = classIndex;
		}
	}

	results[point_index] = min_class_index;
}

__global__ void getClassesForPoints_streams(int *results, double *etalons, double *data, int n, int m, int k, int stream_id)
{
	//stores the distance from the point to the each class
	extern __shared__ double shared_data[];

	auto point_index = blockIdx.x  * blockDim.x + threadIdx.x;
	auto classIdex = threadIdx.y;

	if (point_index >= n) return;

	auto shared_data_index = threadIdx.x * k;

	shared_data[shared_data_index + classIdex] = calcDistanceDevice(data + point_index * m + stream_id * n * m, etalons + classIdex * m, m);

	__syncthreads();

	if (classIdex != 0) return;

	auto min_class_index = 0;
	auto min_distance = shared_data[shared_data_index];
	for (auto classIndex = 1; classIndex < k; classIndex++)
	{
		if (shared_data[shared_data_index + classIndex] < min_distance)
		{
			min_distance = shared_data[shared_data_index + classIndex];
			min_class_index = classIndex;
		}
	}

	(results + stream_id * n)[point_index] = min_class_index;
}

__device__ void warpSum(volatile double *shared_data, int tid)
{
	shared_data[tid] += shared_data[tid + 32];
	shared_data[tid] += shared_data[tid + 16];
	shared_data[tid] += shared_data[tid + 8];
	shared_data[tid] += shared_data[tid + 4];
	shared_data[tid] += shared_data[tid + 2];
	shared_data[tid] += shared_data[tid + 1];
}

__global__ void sumEtalonsGPU(int *results, double *input_data, double *output_data, int n, int m)
{
	//the results ordered by classIndex e.g. A1 A2 A3 B1 B2 B3 ...
	extern __shared__ double shared_data[];

	unsigned int threadIndex = threadIdx.x;
	unsigned int classIndex = blockIdx.z;
	unsigned int pointIndex = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned int attributeIndex = blockIdx.y;

	if (pointIndex < n && (results == NULL || results[pointIndex] == classIndex)) {
		unsigned int index = pointIndex * m + attributeIndex;
		if (results == NULL) {
			index += classIndex * n * m;
		}

		shared_data[threadIndex] = input_data[index];
	}
	else {
		shared_data[threadIndex] = 0;
		if (pointIndex >= n) return;
	}

	__syncthreads();


	for (unsigned int s = blockDim.x / 2; s > 32; s >>= 1) {
		if (threadIndex < s) {
			shared_data[threadIndex] += shared_data[threadIndex + s];
		}

		__syncthreads();
	}

	if (threadIndex < 32) {
		warpSum(shared_data, threadIndex);
	}

	if (threadIdx.x == 0) {
		output_data[classIndex * gridDim.x * m + blockIdx.x * m + attributeIndex] = shared_data[0];
	}
}

__global__ void getSizes(int *results, int *sizes, int n) {
	
	auto pointId = blockIdx.x * blockDim.x + threadIdx.x;
	
	if (pointId >= n) return;
	
	atomicAdd(sizes + results[pointId], 1);
}

__global__ void equals(int *array_1, int* array_2, int size, int *result) {

	auto index = blockIdx.x * blockDim.x + threadIdx.x;

	if (index >= size) return;

	if (array_1[index] != array_2[index]) {
		atomicOr(result, 1);
	}
}

void calculateEtalons(int *results, double *etalons, double *points_cuda, int n, int m, int k, int *sizes)
{
	/*cudaStream_t streams[STREAM_COUNT];

	for (int i = 0; i < STREAM_COUNT; i++){
		if (!cudaStreamCreate(streams + i) == cudaSuccess)
			cout << "stream creation failed!" << endl;
	}*/
	
	TimerCuda timer, timerTotal;
	//timerTotal.start();
	int shared_size = sizeof(double) * MAX_THREADS;
	dim3 threads_per_block(MAX_THREADS);
	// x - point index; y - attr index; z - class index
	dim3 blocks_per_grid((n + threads_per_block.x - 1) / threads_per_block.x, m, k);

	double *idata; cudaMalloc(&idata, k * blocks_per_grid.x * m * sizeof(double));
	double *odata; cudaMalloc(&odata, k * blocks_per_grid.x * m * sizeof(double));

	//timer.start();
	sumEtalonsGPU << <blocks_per_grid, threads_per_block, shared_size >> >(results, points_cuda, odata, n, m);
	//timer.logStop("Sum etalons GPU");

	int blockAmount = blocks_per_grid.x;
	while (blockAmount != 1)
	{
		cudaMemcpy(idata, odata, k * blockAmount * m * sizeof(double), cudaMemcpyDeviceToDevice);
		cudaThreadSynchronize();

		blocks_per_grid.x = (blockAmount + threads_per_block.x - 1) / threads_per_block.x;

		sumEtalonsGPU << <blocks_per_grid, threads_per_block, shared_size >> >(NULL, idata, odata, blockAmount, m);

		blockAmount = blocks_per_grid.x;
	}

	cudaMemcpy(etalons, odata, k * m * sizeof(double), cudaMemcpyDeviceToDevice);
	cudaThreadSynchronize();
	
	divArrayGPU << <k, m >> >(etalons, k * m, sizes);

	cudaFree(idata);
	cudaFree(odata);
/*
	for (int i = 0; i < STREAM_COUNT; i++)
		cudaStreamDestroy(streams[i]);*/

	//timerTotal.logStop("Calculate etalons");
}

void recognizeGPU(double *points, int n, int m, int k, double *h_etalons)
{
	TimerCuda timer;

	int points_size = n * m * sizeof(double);
	int etalons_size = k * m * sizeof(double);
	int results_size = n * sizeof(int);
	int sizes_size = k * sizeof(int);

	int *result = new int[1];

	double *points_cuda; cudaMalloc(&points_cuda, points_size);
	double *etalons_cuda; cudaMalloc(&etalons_cuda, etalons_size);
	int *results_cuda; cudaMalloc(&results_cuda, results_size);
	int *sizes_cuda; cudaMalloc(&sizes_cuda, sizes_size);
	int *prev_sizes_cuda; cudaMalloc(&prev_sizes_cuda, sizes_size);
	int *result_cuda; cudaMalloc(&result_cuda, sizeof(int));

	cudaMemcpy(points_cuda, points, points_size, cudaMemcpyHostToDevice);
	cudaMemcpy(etalons_cuda, h_etalons, etalons_size, cudaMemcpyHostToDevice);
	cudaThreadSynchronize();

	//we need to divide to k since there is a limit to threads per block
	dim3 threads_per_block(MAX_THREADS / k, k, 1);
	dim3 blocks_per_grid((n + threads_per_block.x - 1) / threads_per_block.x, 1, 1);

	bool done = false;
	while (!done)
	{
		cudaMemcpy(prev_sizes_cuda, sizes_cuda, sizes_size, cudaMemcpyDeviceToDevice);

		getClassesForPoints << <blocks_per_grid, threads_per_block, threads_per_block.x * k * sizeof(double)>> >(results_cuda, etalons_cuda, points_cuda, n, m, k);

		cudaMemset(sizes_cuda, 0, sizes_size);
		cudaMemset(result_cuda, 0, sizeof(int));
		getSizes << < (n + MAX_THREADS - 1) / MAX_THREADS, MAX_THREADS >> >(results_cuda, sizes_cuda, n);

		equals << <(sizes_size + MAX_THREADS - 1) / MAX_THREADS, MAX_THREADS >> >(prev_sizes_cuda, sizes_cuda, sizes_size, result_cuda);

		cudaMemcpy(result, result_cuda, sizeof(int), cudaMemcpyDeviceToHost);
		if (*result == 0) break;

		timer.start();

		calculateEtalons(results_cuda, etalons_cuda, points_cuda, n, m, k, sizes_cuda);

		timer.stop();
	}

	timer.logTotal("calculateEtalons for points takes");

	cudaMemcpy(h_etalons, etalons_cuda, etalons_size, cudaMemcpyDeviceToHost);

	cudaFree(points_cuda);
	cudaFree(etalons_cuda);
	cudaFree(results_cuda);
}

void recognizeGPU_streams(double *points, int n, int m, int k, double *h_etalons)
{
	TimerCuda timer;
	cudaStream_t streams[STREAM_COUNT];


	for (int i = 0; i < STREAM_COUNT; i++){
		if (!cudaStreamCreate(streams + i) == cudaSuccess)
			cout << "stream creation failed!" << endl;
	}
		
	int points_size = n * m * sizeof(double);
	int etalons_size = k * m * sizeof(double);
	int results_size = n * sizeof(int);
	int sizes_size = k * sizeof(int);
	
	int *result = new int[1];

	double *points_cuda; cudaMalloc(&points_cuda, points_size);
	double *etalons_cuda; cudaMalloc(&etalons_cuda, etalons_size);
	int *results_cuda; cudaMalloc(&results_cuda, results_size);
	int *sizes_cuda; cudaMalloc(&sizes_cuda, sizes_size);
	int *prev_sizes_cuda; cudaMalloc(&prev_sizes_cuda, sizes_size);
	int *result_cuda; cudaMalloc(&result_cuda, sizeof(int));

	//we need to divide to k since there is a limit to threads per block
	dim3 threads_per_block(MAX_THREADS / k, k, 1);
	dim3 blocks_per_grid((((n + threads_per_block.x - 1) / threads_per_block.x) + STREAM_COUNT - 1) / STREAM_COUNT, 1, 1);

	auto get_sizes_block_size = (((n + MAX_THREADS - 1) / MAX_THREADS) + STREAM_COUNT - 1) / STREAM_COUNT;
	auto equals_block_size = (((sizes_size + MAX_THREADS - 1) / MAX_THREADS) + STREAM_COUNT - 1) / STREAM_COUNT;

	//cudaMemcpy(points_cuda, points, points_size, cudaMemcpyHostToDevice);
	cudaMemcpy(etalons_cuda, h_etalons, etalons_size, cudaMemcpyHostToDevice);

	auto size_per_stream = points_size / STREAM_COUNT;
	for (auto i = 0; i < STREAM_COUNT; i++) {
		cudaMemcpyAsync(points_cuda + i * size_per_stream, points + i * size_per_stream, size_per_stream, cudaMemcpyHostToDevice, streams[i]);
		getClassesForPoints_streams << <blocks_per_grid, threads_per_block, threads_per_block.x * k * sizeof(double), streams[i] >> >(
			results_cuda,
			etalons_cuda,
			points_cuda,
			n / STREAM_COUNT, m, k, i);
	}

	for (auto i = 0; i < STREAM_COUNT; i++) {
		cudaStreamSynchronize(streams[i]);
	}

	bool done = false;
	bool isFirstIter = true;
	while (!done)
	{
		cudaMemcpy(prev_sizes_cuda, sizes_cuda, sizes_size, cudaMemcpyDeviceToDevice);

		//for (auto i = 0; i < STREAM_COUNT; i++) {
			/*if (isFirstIter){
				cudaMemcpy(points_cuda + i * size_per_stream, points + i * size_per_stream, size_per_stream, cudaMemcpyHostToDevice);
			}*/
		getClassesForPoints<< <(n + threads_per_block.x - 1) / threads_per_block.x, threads_per_block, threads_per_block.x * k * sizeof(double) >> >(
				results_cuda,
				etalons_cuda,
				points_cuda,
				n, m, k);
			//cudaStreamSynchronize(streams[i]);
		//}
		//isFirstIter = false;


		for (auto i = 0; i < STREAM_COUNT; i++) {
			cudaStreamSynchronize(streams[i]);
		}

		cudaMemset(sizes_cuda, 0, sizes_size);
		cudaMemset(result_cuda, 0, sizeof(int));

		//for (auto i = 0; i < STREAM_COUNT; i++) {
			getSizes << <(n + MAX_THREADS - 1) / MAX_THREADS, MAX_THREADS>> >(results_cuda, sizes_cuda, n);
		//}

		//for (auto i = 0; i < STREAM_COUNT; i++) {
			equals << <(sizes_size + MAX_THREADS - 1) / MAX_THREADS, MAX_THREADS>> >(prev_sizes_cuda, sizes_cuda, sizes_size, result_cuda);
		//}

		cudaMemcpy(result, result_cuda, sizeof(int), cudaMemcpyDeviceToHost);
		if (*result == 0) break;
		
		timer.start();

		calculateEtalons(results_cuda, etalons_cuda, points_cuda, n, m, k, sizes_cuda);

		timer.stop();
	}
	//timer.logTotal("calculateEtalons for points takes");

	cudaMemcpy(h_etalons, etalons_cuda, etalons_size, cudaMemcpyDeviceToHost);

	cudaFree(points_cuda);
	cudaFree(etalons_cuda);
	cudaFree(results_cuda);

	for (int i = 0; i < STREAM_COUNT; i++)
		cudaStreamDestroy(streams[i]);
}

////////////////////////////
//CPU

double calcDistanceCPU(int m, double *p1, double *p2)
{
	double d = 0;

	for (auto i = 0; i < m; i++) {
		d += POW_2(p1[i] - p2[i]);
	}

	return sqrt(d);
}

double* initEtalonsCPU(double *data, int n, int m, int k)
{
	auto etalons = new double[k * m];																												

	auto initialSize = n / k;
	for (auto classIndex = 0; classIndex < k; classIndex++)
	{
		for (auto attr = 0; attr < m; attr++)
		{
			auto attrIndex = classIndex * m + attr;
			auto pointsOffset = classIndex * initialSize * m;
			for (auto r = 0; r < initialSize; r++) {
				etalons[attrIndex] += data[attr + r * m + pointsOffset];
			}
			etalons[attrIndex] /= initialSize;
		}
	}

	return etalons;
}

void calcEtalonsCPU(double **claster, int size, double *etalon, int m)
{
	for (auto attr = 0; attr < m; attr++) {
		etalon[attr] = 0;
	}

	for (auto i = 0; i < size; i++)
	{
		for (auto attr = 0; attr < m; attr++)
		{
			etalon[attr] += claster[i][attr];
		}
	}

	for (auto attr = 0; attr < m; attr++) {
		etalon[attr] /= size;
	}
}

int getClassCPU(double *etalons, int k, double *point, int m)
{
	int min = -1;
	double minD = 0;
	int j;

	for (j = 0; j < k; j++)
	{
		double d = calcDistanceCPU(m, point, etalons + j * m);

		if (j == 0)
		{
			min = 0;
			minD = d;
		}
		else if (d < minD)
		{
			minD = d;
			min = j;
		}
	}

	return min;
}

//returns etalons
void recognizeCPU(double *points, double* etalons, int n, int m, int k)
{
	double **clasters = new double*[k * n];

	int *sizes = new int[k];
	int *prev_sizes = new int[k];

	bool done = false;
	while (!done)
	{
		for (auto i = 0; i < k; i++)
		{
			prev_sizes[i] = sizes[i];
			sizes[i] = 0;
		}

		for (auto pointIndex = 0; pointIndex < n; pointIndex++)
		{
			auto point = points + pointIndex * m;
			auto classIndex = getClassCPU(etalons, k, point, m);

			clasters[classIndex * n + sizes[classIndex]] = point;
			sizes[classIndex] += 1;
		}

		done = memcmp(sizes, prev_sizes, k * sizeof(int)) == 0;
		if (done) break;



		//////DEBUG
		/*for (auto i = 0; i < k; i++) {
			cout << sizes[i] << " ";
		}
		cout << endl;*/
		//////

		for (auto classIndex = 0; classIndex < k; classIndex++) {
			calcEtalonsCPU(clasters + classIndex * n, sizes[classIndex], etalons + classIndex * m, m);
		}
	}

	delete sizes;
	delete prev_sizes;
	delete clasters;
}

void classifyCPU(double *features, double *clasters, int n, int m, int k)
{
	int *sizes = new int[k]();

	for (auto i = 0; i < n; i++) {
		sizes[getClassCPU(clasters, k, features + i*m, m)] += 1;
	}

	for (auto i = 0; i < k; i++) {
		cout << i << " : " << sizes[i] << endl;
	}

	free(sizes);
}

///////////////////////////

int main()
{
	cudaSetDevice(0);
	Timer timer;
	float ms = 0;
	
	timer.start();
	cout << "Reading data..." << endl;

	double *points = readData();
	
	timer.logEnd("Reading data takes");

	// CPU
	
	double *etalons = initEtalonsCPU(points, N, M, K);
	cout << "CPU started" << endl;

	TimerCuda timerC;

	timerC.start();
	recognizeCPU(points, etalons, N, M, K);
	timerC.logStop("CPU takes"); 

	//classifyCPU(points, etalons, N, M, K);

	etalons = initEtalonsCPU(points, N, M, K);

	cout << "CUDA started:" << endl;

	timerC.start();
	recognizeGPU(points, N, M, K, etalons);
	timerC.logStop("CUDA takes");

	classifyCPU(points, etalons, N, M, K);

	etalons = initEtalonsCPU(points, N, M, K);
	cout << "CUDA STREAMS started:" << endl;

	timerC.start();
	recognizeGPU_streams(points, N, M, K, etalons);
	timerC.logStop("CUDA STREAMS takes");

	classifyCPU(points, etalons, N, M, K);

	delete points;
	delete etalons;

	system("pause");
	return 0;
}
