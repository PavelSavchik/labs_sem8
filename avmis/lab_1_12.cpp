#include <iostream>
#include <chrono>
#include <malloc.h>
#include <stdlib.h>
#include <time.h>
#include <omp.h>

using namespace std;

struct Matrix {
	double **data;
	int rows;
	int cols;
};

#define ROWS 10
#define COLS 10
#define INNER_MATRIX_ROWS 12
#define INNER_MATRIX_COLS 12
#define FOR_I_J for(int i = 0; i < ROWS; i++) for(int j = 0; j < COLS; j++)

void mul_matrix(Matrix* A, Matrix* B, Matrix* C);
void mul_matrices(Matrix*** A, Matrix*** B, Matrix ***C);
void sum_matrix(Matrix *A, Matrix *B, Matrix *C);

void mul_matrix_no_vect(Matrix* A, Matrix* B, Matrix* C);
void mul_matrices_no_vect(Matrix*** A, Matrix*** B, Matrix ***C);
void sum_matrix_no_vect(Matrix *A, Matrix *B, Matrix *C);

void mul_matrix_manually(Matrix* A, Matrix* B, Matrix* C);
void mul_matrices_manually(Matrix*** A, Matrix*** B, Matrix ***C);
void sum_matrix_manually(Matrix *A, Matrix *B, Matrix *C);

void mul_matrix(Matrix* A, Matrix* B, Matrix* C);
void mul_matrices_openmp(Matrix*** A, Matrix*** B, Matrix ***C);
void sum_matrix(Matrix *A, Matrix *B, Matrix *C);

Matrix*** new_matrices(int rows, int cols, int inner_rows, int inner_cols);
void initialize_matrix(Matrix *matrix, bool makeNull);
void initialize_matrices(Matrix ***matrices, bool makeNull = false);
Matrix* new_matrix(int rows, int cols);
void delete_matrices(Matrix ***matrices);
void print_matrix(Matrix *matrix);

int main() {
	omp_set_dynamic(0);
	omp_set_num_threads(10);
	Matrix ***A = new_matrices(ROWS, COLS, INNER_MATRIX_ROWS, INNER_MATRIX_COLS);
	initialize_matrices(A);
	Matrix ***B = new_matrices(ROWS, COLS, INNER_MATRIX_ROWS, INNER_MATRIX_COLS);
	initialize_matrices(B);
	Matrix ***C = new_matrices(ROWS, COLS, INNER_MATRIX_ROWS, INNER_MATRIX_COLS);
	initialize_matrices(C, true);

	auto timer = chrono::high_resolution_clock();
	auto start = timer.now().time_since_epoch().count();
	auto end = timer.now().time_since_epoch().count();

	start = timer.now().time_since_epoch().count();
	mul_matrices_no_vect(A, B, C);
	end = timer.now().time_since_epoch().count();
	cout << "No vect mul completed in " << (end - start) / (double)1000000 << " milliseconds" << endl;

	delete_matrices(C);
	C = new_matrices(ROWS, COLS, INNER_MATRIX_ROWS, INNER_MATRIX_COLS);
	initialize_matrices(C, true);

	start = timer.now().time_since_epoch().count();
	mul_matrices(A, B, C);
	end = timer.now().time_since_epoch().count();
	cout << "Auto vect mul completed in " << (end - start) / (double)1000000 << " milliseconds" << endl;

	delete_matrices(C);
	C = new_matrices(ROWS, COLS, INNER_MATRIX_ROWS, INNER_MATRIX_COLS);
	initialize_matrices(C, true);


	start = timer.now().time_since_epoch().count();
	mul_matrices_manually(A, B, C);
	end = timer.now().time_since_epoch().count();
	cout << "Handle optimized mul completed in " << (end - start) / (double)1000000 << " milliseconds" << endl;

	delete_matrices(C);
	C = new_matrices(ROWS, COLS, INNER_MATRIX_ROWS, INNER_MATRIX_COLS);
	initialize_matrices(C, true);

	start = timer.now().time_since_epoch().count();
	mul_matrices_openmp(A, B, C);
	end = timer.now().time_since_epoch().count();
	cout << "OpenMP mul completed in " << (end - start) / (double)1000000 << " milliseconds" << endl;

	delete_matrices(A);
	delete_matrices(B);
	delete_matrices(C);
	system("pause");
}

void mul_matrix(Matrix* A, Matrix* B, Matrix* C) {
	auto adata = A->data;
	auto bdata = B->data;
	auto cdata = C->data;
	auto cols = A->cols;
	for (int i = 0; i < INNER_MATRIX_ROWS; i++) {
		for (int j = 0; j < INNER_MATRIX_COLS; j++) {
			auto a = adata[i][j];
			for (int r = 0; r < INNER_MATRIX_COLS; r++) {
				cdata[i][r] += a * bdata[j][r];
			}
		}
	}
}

void sum_matrix(Matrix *A, Matrix *B, Matrix *C) {
	auto cols = B->cols;
	auto adata = A->data;
	auto bdata = B->data;
	auto cdata = C->data;
	for (int i = 0; i < INNER_MATRIX_ROWS; i++) {
		for (int j = 0; j < INNER_MATRIX_COLS; j++) {
			cdata[i][j] = bdata[i][j] + adata[i][j];
		}
	}
}

//result in the C matrix
void mul_matrices(Matrix*** A, Matrix*** B, Matrix ***C) {
	Matrix *temp = new_matrix(A[0][0]->rows, A[0][0]->cols);
	FOR_I_J{
		//auto adata = A[i][j]->data;
		for (int r = 0; r < COLS; r++) {
			//auto bdata = B[j][r]->data;
			//auto cdata = C[i][r]->data;
			initialize_matrix(temp, true);
			mul_matrix(A[i][j], B[j][r], temp);

			/*for (int ii = 0; ii < INNER_MATRIX_ROWS; ii++) {
			for (int jj = 0; jj < INNER_MATRIX_COLS; jj++) {
			auto a = adata[ii][jj];
			for (int rr = 0; rr < INNER_MATRIX_COLS; rr++) {
			cdata[ii][rr] += a * bdata[jj][rr];
			}
			}
			}
			for (int ii = 0; ii < A[i][j]->rows; ii++) {
			for (int jj = 0; jj < INNER_MATRIX_COLS; jj++) {
			cdata[ii][jj] = bdata[ii][jj] + adata[ii][jj];
			}
			}*/

			sum_matrix(temp, C[i][r], C[i][r]);
		}
	}
}

void mul_matrix_no_vect(Matrix* A, Matrix* B, Matrix* C) {
#pragma loop(no_vector)
	for (int i = 0; i < INNER_MATRIX_ROWS; i++) {
#pragma loop(no_vector)
		for (int j = 0; j < INNER_MATRIX_COLS; j++) {
#pragma loop(no_vector)
			for (int r = 0; r < INNER_MATRIX_ROWS; r++) {
				C->data[i][r] += A->data[i][j] * B->data[j][r];
			}
		}
	}
}

void sum_matrix_no_vect(Matrix *A, Matrix *B, Matrix *C) {
#pragma loop(no_vector)
	for (int i = 0; i < INNER_MATRIX_ROWS; i++) {
#pragma loop(no_vector)
		for (int j = 0; j < INNER_MATRIX_COLS; j++) {
			C->data[i][j] = A->data[i][j] + B->data[i][j];
		}
	}
}

//result in the C matrix
void mul_matrices_no_vect(Matrix*** A, Matrix*** B, Matrix ***C) {
	Matrix *temp = new_matrix(A[0][0]->rows, A[0][0]->cols);
#pragma loop(no_vector)
	FOR_I_J{
#pragma loop(no_vector)
		for (int r = 0; r < COLS; r++) {
			initialize_matrix(temp, true);
			mul_matrix_no_vect(A[i][j], B[j][r], temp);
			sum_matrix_no_vect(temp, C[i][r], C[i][r]);
		}
	}
}

void mul_matrix_manually(Matrix* A, Matrix* B, Matrix* C) {
	for (int i = 0; i < INNER_MATRIX_ROWS; i++) {
		double * temp = C->data[i];
		double buffer;

		__m256d c1 = _mm256_load_pd(temp);
		__m256d c2 = _mm256_load_pd(temp + 4);
		__m256d c3 = _mm256_load_pd(temp + 8);
		for (int j = 0; j < INNER_MATRIX_COLS; j++) {
			__m256d a = _mm256_set1_pd(A->data[i][j]);
			__m256d b1 = _mm256_load_pd(B->data[j]);
			__m256d b2 = _mm256_load_pd(B->data[j] + 4);
			__m256d b3 = _mm256_load_pd(B->data[j] + 8);
			b1 = _mm256_mul_pd(b1, a);
			b2 = _mm256_mul_pd(b2, a);
			b3 = _mm256_mul_pd(b3, a);
			c1 = _mm256_add_pd(c1, b1);
			c2 = _mm256_add_pd(c2, b2);
			c3 = _mm256_add_pd(c3, b3);
		}

		_mm256_store_pd(temp, c1);
		_mm256_store_pd(temp + 4, c2);
		_mm256_store_pd(temp + 8, c3);
	}
}

void sum_matrix_manually(Matrix *A, Matrix *B, Matrix *C) {
	__m256d a1, a2, a3, b1, b2, b3;
	double * temp;
	double buffer;
	for (int i = 0; i < A->rows; i++) {
		temp = C->data[i];

		a1 = _mm256_load_pd(A->data[i]);
		a2 = _mm256_load_pd(A->data[i] + 4);
		a3 = _mm256_load_pd(A->data[i] + 8);
		b1 = _mm256_load_pd(B->data[i]);
		b2 = _mm256_load_pd(B->data[i] + 4);
		b3 = _mm256_load_pd(B->data[i] + 8);

		a1 = _mm256_add_pd(a1, b1);
		a2 = _mm256_add_pd(a2, b2);
		a3 = _mm256_add_pd(a3, b2);
		_mm256_store_pd(temp, a1);
		_mm256_store_pd(temp + 4, a2);
		_mm256_store_pd(temp + 8, a3);
	}
}

void mul_matrices_manually(Matrix*** A, Matrix*** B, Matrix ***C) {
	Matrix *temp = new_matrix(A[0][0]->rows, A[0][0]->cols);
	FOR_I_J{
		for (int r = 0; r < COLS; r++) {
			initialize_matrix(temp, true);
			mul_matrix_manually(A[i][j], B[j][r], temp);
			sum_matrix_manually(temp, C[i][r], C[i][r]);
		}
	}
}

void mul_matrices_openmp(Matrix*** A, Matrix*** B, Matrix ***C) {
	int i, j, r;
#pragma omp parallel shared(A, B, C) private(i, j, r)
	{
		Matrix *temp = new_matrix(A[0][0]->rows, A[0][0]->cols);
#pragma omp for
		for (i = 0; i < ROWS; i++) {
			for (j = 0; j < COLS; j++) {
				for (r = 0; r < COLS; r++) {
					initialize_matrix(temp, true);
					mul_matrix(A[i][j], B[j][r], temp);
					sum_matrix(temp, C[i][r], C[i][r]);
				}
			}
		}
	}
}

void print_matrix(Matrix *matrix) {
	for (int i = 0; i < matrix->rows; i++) {
		for (int j = 0; j < INNER_MATRIX_COLS; j++) {
			cout << matrix->data[i][j] << " ";
		}
		cout << endl;
	}
	cout << endl;
}

Matrix*** new_matrices(int rows, int cols, int inner_rows, int inner_cols) {
	Matrix ***matrices = (Matrix***)_aligned_malloc(rows * sizeof(Matrix**), 32);
	for (int i = 0; i < rows; i++) {
		matrices[i] = (Matrix**)_aligned_malloc(cols * sizeof(Matrix*), 32);
		for (int j = 0; j < cols; j++) {
			matrices[i][j] = new_matrix(inner_rows, inner_cols);
		}
	}
	return matrices;
}

Matrix* new_matrix(int rows, int cols) {
	Matrix *m = (Matrix*)_aligned_malloc(sizeof(Matrix), 32);

	m->data = (double**)_aligned_malloc(sizeof(double*) * rows, 32);
	for (int row = 0; row < rows; row++) {
		m->data[row] = (double*)_aligned_malloc(sizeof(double) * cols, 32);
	}
	m->rows = rows;
	m->cols = cols;
	return m;
}

void initialize_matrices(Matrix ***matrices, bool makeNull) {
	FOR_I_J{
		initialize_matrix(matrices[i][j], makeNull);
	}
}

void initialize_matrix(Matrix *matrix, bool makeNull) {
	for (int i = 0; i < matrix->rows; i++) {
		for (int j = 0; j < matrix->cols; j++) {
			matrix->data[i][j] = makeNull ? 0 : (double)(rand() % 100 + 1);
			//matrix->data[i][j] = makeNull ? 0 : 1;
		}
	}
}

void delete_matrices(Matrix ***matrices) {
	for (int i = 0; i < ROWS; i++) {
		for (int j = 0; j < COLS; j++) {
			Matrix* m = matrices[i][j];
			for (int j = 0; j < m->rows; j++) {
				_aligned_free(m->data[j]);
			}
			_aligned_free(m->data);
			_aligned_free(matrices[i][j]);
		}
		_aligned_free(matrices[i]);
	}
	_aligned_free(matrices);
}