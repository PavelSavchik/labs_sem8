#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <time.h>
#include <fstream>

using namespace std;

#define repeat(n) for(int i=0;i<n;i++)

#define KB *1024
#define MB *1048576
#define integers /sizeof(int)
#define of

#define CACHE_L1_SIZE (128  KB)

void travel(int * mem, int steps);
void place_data(int * mem, int offset, int fragments);

int main()
{
	int mem_size = 100 MB of integers;
	int offset = 1 MB of integers;
	int* mem = (int*)malloc(100 MB);
	fstream file;
	file.open("output.txt", fstream::out | fstream::trunc);

	for (int n = 1; n < 20; n++)
	{
		place_data(mem, offset, n);

		clock_t begin = clock();
		travel(mem, 10000000);
		clock_t end = clock();

		file << double(end - begin) / CLOCKS_PER_SEC << '\t';
		printf("%d: %f\n", n, double(end - begin) / CLOCKS_PER_SEC);
	}
	file << endl;
	file.close();
	system("pause");
}

void travel(int * mem, int steps)
{
	register int val = 0;
	register int addr = 0;
	repeat(steps)
	{
		addr = mem[addr];
	}
}

void place_data(int * mem, int offset, int fragments)
{
	int items_total = CACHE_L1_SIZE of integers;
	items_total = (items_total / fragments) * fragments;
	int bank_size = items_total / fragments;
	repeat(items_total)
	{
		int row = i / bank_size;
		int col = i % bank_size;
		int addr = row * offset + col;
		if (row < fragments - 1)
			mem[addr] = addr + offset;
		else if (i != items_total - 1)
			mem[addr] = col + 1;
		else
			mem[addr] = 0;
	}
}