#include <stdio.h>
#include <iostream>
#include <intrin.h>
#include <Windows.h>

using namespace std;

void main() {
	SetPriorityClass(GetCurrentProcess(), HIGH_PRIORITY_CLASS);
	auto n = 10000L;
	auto i = 0;
	long long result = 0;
	__int32 a = 0, b = 0;
	result = __rdtsc();

	for (i; i < n; i++) {
		__asm add eax, ebx
		__asm add eax, ebx
		__asm add eax, ebx
		__asm add eax, ebx
		__asm add eax, ebx
		__asm add eax, ebx
		__asm add eax, ebx
		__asm add eax, ebx
		__asm add eax, ebx
		__asm add eax, ebx
		__asm add eax, ebx
		__asm add eax, ebx
		__asm add eax, ebx
		__asm add eax, ebx
		__asm add eax, ebx
		__asm add eax, ebx
		__asm add eax, ebx
		__asm add eax, ebx
		__asm add eax, ebx
		__asm add eax, ebx
		__asm add eax, ebx
		__asm add eax, ebx
		__asm add eax, ebx
		__asm add eax, ebx
		__asm add eax, ebx
		__asm add eax, ebx
		__asm add eax, ebx
		__asm add eax, ebx
		__asm add eax, ebx
		__asm add eax, ebx
		__asm add eax, ebx
		__asm add eax, ebx
		__asm add eax, ebx
		__asm add eax, ebx
		__asm add eax, ebx
		__asm add eax, ebx
		__asm add eax, ebx
		__asm add eax, ebx
		__asm add eax, ebx
		__asm add eax, ebx
	}

	result = __rdtsc() - result;
	
	cout << "latency  " << double((result / n)) / 40 << endl;

	result = __rdtsc();

	for (i = 0; i < n; i++) {
		__asm add ebx, eax
		__asm add ecx, eax
		__asm add edx, eax
		__asm add esi, eax
		__asm add edi, eax
		__asm add ebx, eax
		__asm add ecx, eax
		__asm add edx, eax
		__asm add esi, eax
		__asm add edi, eax
		__asm add ebx, eax
		__asm add ecx, eax
		__asm add edx, eax
		__asm add esi, eax
		__asm add edi, eax
		__asm add ebx, eax
		__asm add ecx, eax
		__asm add edx, eax
		__asm add esi, eax
		__asm add edi, eax
		__asm add ebx, eax
		__asm add ecx, eax
		__asm add edx, eax
		__asm add esi, eax
		__asm add edi, eax
		__asm add ebx, eax
		__asm add ecx, eax
		__asm add edx, eax
		__asm add esi, eax
		__asm add edi, eax
		__asm add ebx, eax
		__asm add ecx, eax
		__asm add edx, eax
		__asm add esi, eax
		__asm add edi, eax
		__asm add ebx, eax
		__asm add ecx, eax
		__asm add edx, eax
		__asm add esi, eax
		__asm add edi, eax
		__asm add ebx, eax
		__asm add ecx, eax
		__asm add edx, eax
		__asm add esi, eax
		__asm add edi, eax
		__asm add ebx, eax
		__asm add ecx, eax
		__asm add edx, eax
		__asm add esi, eax
		__asm add edi, eax
	}

	result = __rdtsc() - result;

	cout << "throughput  " << double(result / n) / 50. << endl;

	system("pause");
}