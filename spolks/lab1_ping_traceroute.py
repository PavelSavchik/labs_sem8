import socket
import struct
import sys
import time
import select
import os
import pwd

ICMP_PACKET_FORMAT = "BBHHH"
IP_HEADER = 'BBHHHBBH4s4s'
MSG_PEEK = 2

#spizheno
def checksum(source_string):
    sum_ = 0
    count_to = (len(source_string) / 2) * 2
    count = 0
    while count < count_to:
        this_val = source_string[count + 1]*256 + source_string[count]
        sum_ += this_val
        sum_ &= 0xffffffff  # Necessary?
        count += 2
    if count_to < len(source_string):
        sum_ += source_string[len(source_string) - 1]
        sum_ &= 0xffffffff  # Necessary?
    sum_ = (sum_ >> 16) + (sum_ & 0xffff)
    sum_ += (sum_ >> 16)
    answer = ~sum_
    answer &= 0xffff
    # Swap bytes. Bugger me if I know why.
    answer = answer >> 8 | (answer << 8 & 0xff00)
    return answer

def createIPHeader(id, ipSource, ipDestination):
    ip_ihl = 5
    ip_ver = 4
    ip_tos = 0
    ip_tot_len = 28
    ip_id = id
    ip_frag_off = 0
    ip_ttl = 64
    ip_proto = 1
    ip_saddr = socket.inet_aton(ipSource)
    ip_daddr = socket.inet_aton(ipDestination)

    ip_ihl_ver = (ip_ver << 4) + ip_ihl

    ip_header = struct.pack(IP_HEADER, ip_ihl_ver, ip_tos, ip_tot_len, ip_id, ip_frag_off,
                            ip_ttl, ip_proto, 0, ip_saddr, ip_daddr)
    # ip_header = struct.pack(IP_HEADER, ip_ihl_ver, ip_tos, ip_tot_len, ip_id, ip_frag_off,
    #                         ip_ttl, ip_proto, socket.htons(checksum(ip_header)), ip_saddr, ip_daddr)
    return ip_header


def createICMPPacket(type, code, id, seq, payload = 0):
    #type(8) code (8) checksum(16) id (16) + seq(16)
    icmp_header = struct.pack(ICMP_PACKET_FORMAT, type, code, 0, id, seq)
    #data = payload * '*'
    icmp_header = struct.pack(ICMP_PACKET_FORMAT, type, code, socket.htons(checksum(icmp_header)), id, seq)
    return icmp_header

def getECHORequest(id, seq=1):
    return createICMPPacket(8, 0, id, seq)

def ping(socket, address, times = 10, timeout = 1):
    def _ping(id, address = (address, 0)):
        icmpSocket.sendto(getECHORequest(id), address)
        sendTime = time.time()
        ready = select.select([socket], [], [], timeout)
        if len(ready[0]) == 0 : return None
        receiveTime = time.time()
        packet, responseAddress = icmpSocket.recvfrom(64, MSG_PEEK)
        receivedId = struct.unpack(ICMP_PACKET_FORMAT, packet[20:28])[3]
        if receivedId != id:
            return None
        icmpSocket.recvfrom(64)
        return (receiveTime - sendTime, len(packet))

    def _parseResult(result):
        if result == None :
            print("Timeout is exceeded or packet with wrong id is received.")
        else:
            delay, packetSize = result
            delay = round(delay * 1000, 4);
            print("Received {} bytes from {} with {} ms delay.".format(packetSize, destinationAddress, delay))

    for id in range(times):
        _parseResult(_ping(id))
        time.sleep(1)

def spoof(icmpSocket, destinationAddress, sourceAddress):

    icmpSocket.setsockopt(socket.IPPROTO_IP, socket.IP_HDRINCL, 1)

    def _ping(id, address = (destinationAddress, 0)):
        icmpSocket.sendto(createIPHeader(id, sourceAddress, destinationAddress) + getECHORequest(id), address)
        sendTime = time.time()
        ready = select.select([icmpSocket], [], [], 2)
        if len(ready[0]) == 0 : return None
        receiveTime = time.time()
        packet, responseAddress = icmpSocket.recvfrom(64, MSG_PEEK)
        print(packet)
        receivedId = struct.unpack(ICMP_PACKET_FORMAT, packet[20:28])[3]
        if receivedId != id:
            return None
        icmpSocket.recvfrom(64)
        return (receiveTime - sendTime, len(packet))

    def _parseResult(result):
        if result == None:
            print("Timeout is exceeded or packet with wrong id is received.")
        else:
            delay, packetSize = result
            delay = round(delay * 1000, 4);
            print("Received {} bytes from {} with {} ms delay.".format(packetSize, destinationAddress, delay))

    for id in range(5):
        _parseResult(_ping(id))
        time.sleep(1)


def traceRoute(icmpSocket, address, timeout = 5, port = 22222, tries = 5):
    for ttl in range(1, 64):
        print("TTL {}".format(ttl))
        icmpSocket.setsockopt(socket.SOL_IP, socket.IP_TTL, ttl)
        found = True
        for _ in range(tries):
            icmpSocket.sendto(getECHORequest(ttl), (address, port))
            ready = select.select([icmpSocket], [], [], timeout)
            if len(ready[0]) == 0:
                print('*')
                continue
            else: found = True
            data, responseAddress = icmpSocket.recvfrom(64)
            #print(responseAddress[0], struct.unpack(ICMP_PACKET_FORMAT, data[20:28]))
            print(responseAddress[0])
            if responseAddress[0] == address:
                print("End")
                return
            break
        if not found:
            print("Can't find route")
            return

if __name__ == '__main__':
    try:
        destinationAddress = socket.gethostbyname(sys.argv[2])
        print("{} {}:".format(sys.argv[1], destinationAddress))
        icmpSocket = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_RAW)
        icmpSocket.bind(("", 0))

        #use current user rights
        login = os.getlogin()
        id = pwd.getpwnam(login).pw_uid
        os.setuid(id)

        if sys.argv[1] == "ping":
            ping(icmpSocket, destinationAddress)
        elif sys.argv[1] == "traceroute":
            traceRoute(icmpSocket, destinationAddress)
        elif sys.argv[1] == "spoof":
            spoof(icmpSocket, destinationAddress, socket.gethostbyname(sys.argv[3]))
        icmpSocket.close()
    except socket.gaierror:
        print("Cannot resolve " + sys.argv[2])