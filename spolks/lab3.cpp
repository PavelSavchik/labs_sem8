#include "mpi.h"
#include <iostream>
#include <chrono>
#include <stdlib.h>
#include <time.h>
#include <fstream>

using namespace std;

#define ROOT 0
#define MESSAGE_TAG 1
#define FOR_I_J(n, m) for(int i = 0; i < n; i++) for(int j = 0; j < m; j++)

void print_matrix(int *matrix, int size);
void save_to_file(ofstream &file, int* matrix, int m, int n);

int main(int argc, char** argv) {
	char notification = '&';
	int matrix_size, group_count;

	if (argc < 3) {
		cout << "Not enough args =(" << endl;
		return 0;
 	} else {
 		matrix_size = atoi(argv[1]);
		group_count = atoi(argv[2]);
 	}

	MPI_Init(&argc, &argv);

	int global_rank, process_count;
	MPI_Comm_rank(MPI_COMM_WORLD, &global_rank);
	MPI_Comm_size(MPI_COMM_WORLD, &process_count);

	int group_tag = global_rank % group_count;
	MPI_Comm group;
	MPI_Comm_split(MPI_COMM_WORLD, group_tag, global_rank, &group);

	int group_rank, group_size;
	MPI_Comm_rank(group, &group_rank);
	MPI_Comm_size(group, &group_size);

	int size_to_process = matrix_size / group_size;	//allows to use only even sizes

	int items_amount = matrix_size * matrix_size;
	int process_items_amount = size_to_process * matrix_size;

	int *matrix_a, *matrix_b, *local_a, *local_c;
	matrix_a = new int[items_amount];
	matrix_b = new int[items_amount];
	local_a = new int[process_items_amount];
	local_c = new int[process_items_amount]();

	MPI_Status status;

	if (global_rank == ROOT) {
		cout << "Matrix size: " << matrix_size << endl << "Groups count: " << group_count << endl;
		cout << "Process count: " << process_count << endl;
	}

	if (group_rank == ROOT) {	
		ifstream file_a, file_b;
		file_a.open("matrix_a.txt");
		file_b.open("matrix_b.txt");
		FOR_I_J(matrix_size, matrix_size){
			file_a >> matrix_a[i * matrix_size + j];	
			file_b >> matrix_b[i * matrix_size + j];
		}
		file_a.close();
		file_b.close();

		// print_matrix(matrix_a, matrix_size);
		// print_matrix(matrix_b, matrix_size);
	}

	MPI_Bcast(matrix_b, items_amount, MPI_INT, ROOT, MPI_COMM_WORLD);
	MPI_Scatter(matrix_a, process_items_amount, MPI_INT, local_a, process_items_amount, MPI_INT, ROOT, group);

	auto timer = chrono::high_resolution_clock();
	auto result_file_name = ("result" + to_string(group_tag) + ".bin").c_str();

	MPI_Barrier(group);
	auto start = timer.now().time_since_epoch().count();

	FOR_I_J(size_to_process, matrix_size) {
		for(int k = 0; k < matrix_size; k++) {
			local_c[i * matrix_size + j] += local_a[i * matrix_size + k] * matrix_b[k * matrix_size + j];
		}
	}

	MPI_Barrier(group);
	if (group_rank == ROOT) {
		auto end = timer.now().time_since_epoch().count();
		cout << endl << (end - start) / (double)1000000 << " ms" << endl;
		cout << "result in the " << result_file_name << endl;
		MPI_File_delete(result_file_name, MPI_INFO_NULL);
	} 

	MPI_File output_file;
    MPI_File_open(group, result_file_name, MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &output_file);
    MPI_File_seek(output_file, process_items_amount * group_rank * sizeof(int), MPI_SEEK_SET);
    MPI_File_write(output_file, local_c, process_items_amount, MPI_INT, &status);
    MPI_File_close(&output_file);

	MPI_Finalize();
	return 0;
}

void print_matrix(int *matrix, int size) {
	cout << endl;
	for(int i = 0; i < size; i++) {
		for(int j = 0; j < size; j++) {
			cout << matrix[i * size + j] << " ";
		}
		cout << endl;
	}
}